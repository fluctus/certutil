package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"flag"
	"fmt"
	"log"
	"math/big"
	"os"
	"time"
)

var (
	CAkey     string
	CAcert    string
	ou        string
	cn        string
	sans      string
	server    bool
	client    bool
	key       string
	cert      string
	validfrom string
	validity  int64
)

func init() {
	flag.StringVar(&CAkey, "ca-key", "", "path to CA private key")
	flag.StringVar(&CAcert, "ca", "", "path to CA certificate")
	flag.StringVar(&ou, "ou", "", "Organizational Unit")
	flag.StringVar(&cn, "cn", "", "Common Name")
	flag.StringVar(&sans, "sans", "", "Subject Alternative Names (DNS Names, comma-separated)")
	flag.StringVar(&key, "key", "", "path to certificate private key")
	flag.StringVar(&cert, "cert", "", "path to certificate")
	flag.BoolVar(&server, "server", false, "generate a server cert")
	flag.BoolVar(&client, "client", false, "generate a client cert")
	flag.StringVar(&validfrom, "validfrom", "", "Valid From (in RFC3339 representation)")
	flag.Int64Var(&validity, "validity", 0, "certificate validity, in seconds")
}

func main() {
	flag.Parse()
	command := flag.Arg(0)
	switch command {
	case "genca":
		genca()
	case "gencert":
		fmt.Println("Generating cert")
		gencert()
	default:
		usage()
	}
}

func gencert() {
	if CAkey == "" || CAcert == "" || ou == "" || cn == "" || key == "" || cert == "" || validity == 0 || (server == client) {
		fmt.Fprintln(os.Stderr, "gencert requires -ca-key, -ca, -ou, -cn, -key, -cert, -validity and only one of -server or -client")
		os.Exit(1)
	}

	ca_pair, err := tls.LoadX509KeyPair(CAcert, CAkey)
	if err != nil {
		log.Fatalf("Couldn't read CA cert and key: %s\n", err)
	}

	ca_cert, err := x509.ParseCertificate(ca_pair.Certificate[0])
	if err != nil {
		log.Fatalf("Couldn't parse CA certificate: %s\n", err)
	}

	ca_priv := ca_pair.PrivateKey

	priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		log.Fatalf("Couldn't generate private key: %s\n", err)
	}
	var extKeyUsage x509.ExtKeyUsage
	if server {
		extKeyUsage = x509.ExtKeyUsageServerAuth
	} else {
		extKeyUsage = x509.ExtKeyUsageClientAuth
	}

	notBefore := time.Now()
	if validfrom != "" {
		notBefore, err = time.Parse(time.RFC3339, validfrom)
		if err != nil {
			log.Fatalf("Error parsing validfrom value: %s\n", err)
		}
	}
	notAfter := notBefore.Add(time.Duration(validity) * time.Second)
	serialNumber := big.NewInt(0)
	var DNSNames []string
	if len(sans) > 0 {
		DNSNames = sans.Split(",")
	}

	csr := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			OrganizationalUnit: []string{ou},
			CommonName:         cn,
		},
		NotBefore:   notBefore,
		NotAfter:    notAfter,
		KeyUsage:    x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage: []x509.ExtKeyUsage{extKeyUsage},
		DNSNames:    DNSNames,
	}
	der, err := x509.CreateCertificate(rand.Reader, &csr, ca_cert, &priv.PublicKey, ca_priv)
	if err != nil {
		log.Fatalf("Couldn't create certificate: %s\n", err)
	}
	certOut, err := os.Create(cert)
	if err != nil {
		log.Fatalf("Couldn't open cert file: %s\n", err)
	}
	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: der})
	certOut.Close()
	log.Println("Created cert")
	keyOut, err := os.Create(key)
	if err != nil {
		log.Fatalf("Couldn't open private key: %s\n", err)
	}
	keyBytes, err := x509.MarshalECPrivateKey(priv)
	if err != nil {
		log.Fatalf("Couldn't marshal private key: %s\n", err)
	}
	pem.Encode(keyOut, &pem.Block{Type: "EC PRIVATE KEY", Bytes: keyBytes})
	keyOut.Close()
	log.Println("private key written")
}

func genca() {
	if CAkey == "" || CAcert == "" {
		fmt.Fprintln(os.Stderr, "genca requires -ca-key and -ca")
		os.Exit(1)
	}

	ca_priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		log.Fatalln("Couldn't generate CA private key:", err)
	}
	notBefore := time.Now()
	if validfrom != "" {
		notBefore, err = time.Parse(time.RFC3339, validfrom)
		if err != nil {
			log.Fatalf("Error parsing validfrom value: %s\n", err)
		}
	}
	notAfter := notBefore
	if validity != 0 {
		notAfter = notBefore.Add(time.Duration(validity) * time.Second)
	}
	serialNumber := big.NewInt(0)

	ca_csr := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			OrganizationalUnit: []string{ou},
			CommonName:         cn,
		},
		NotBefore:             notBefore,
		NotAfter:              notAfter,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth},
		BasicConstraintsValid: true,
		IsCA: true,
	}
	pub := &ca_priv.PublicKey
	ca_der, err := x509.CreateCertificate(rand.Reader, &ca_csr, &ca_csr, pub, ca_priv)
	if err != nil {
		log.Fatalf("Couldn't create CA certificate: %s\n", err)
	}
	certOut, err := os.Create(CAcert)
	if err != nil {
		log.Fatalf("Couldn't open CA cert file: %s\n", err)
	}
	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: ca_der})
	certOut.Close()
	log.Println("Created CA cert at", CAcert)
	keyBytes, err := x509.MarshalECPrivateKey(ca_priv)
	if err != nil {
		log.Fatalln("Couldn't marshal CA private key:", err)
	}
	keyOut, err := os.Create(CAkey)
	if err != nil {
		log.Fatalln("Couldn't create file", CAkey, ":", err)
	}
	defer keyOut.Close()
	pem.Encode(keyOut, &pem.Block{Type: "EC PRIVATE KEY", Bytes: keyBytes})
	log.Println("CA private key written at", CAkey)
}

func usage() {
	fmt.Fprintf(os.Stderr, "Usage: %s [options] genca|gencert\n", os.Args[0])
	flag.PrintDefaults()
}
